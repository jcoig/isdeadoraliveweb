import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonAnimatedOneComponent } from './component/ui/button/animated/button-animated-one/button-animated-one.component';

@NgModule({
  declarations: [ ButtonAnimatedOneComponent ],
  imports: [
    CommonModule
  ],
  exports: [
    ButtonAnimatedOneComponent
  ]
})
export class SharedModule { }
