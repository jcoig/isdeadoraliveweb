import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonAnimatedOneComponent } from './button-animated-one.component';

describe('ButtonAnimatedOneComponent', () => {
  let component: ButtonAnimatedOneComponent;
  let fixture: ComponentFixture<ButtonAnimatedOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonAnimatedOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonAnimatedOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
