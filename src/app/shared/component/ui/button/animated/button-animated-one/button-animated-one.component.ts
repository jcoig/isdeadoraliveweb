import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'button-animated-one',
  templateUrl: './button-animated-one.component.html',
  styleUrls: ['./button-animated-one.component.scss']
})
export class ButtonAnimatedOneComponent implements OnInit {

  @Input() text: string;
  
  constructor() { }

  ngOnInit() {
  }

}
