import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Reply } from '../component/model/UserReplay';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  
  private url = 'http://localhost:8085';

  public getRandomCelebrity() {
    return this.http
            .get(`${this.url}/celebrities/random`);

  }

  public async isDead(id: string) {
    const isDead: boolean = <boolean> await this.http.get(`${this.url}/celebrities/is-dead/${id}`).toPromise();
    return isDead ? Reply.DEAD : Reply.ALIVE;
  }
}
