import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit {

  @Input() goalSeconds: number;
  @Output() finished: EventEmitter<void> = new EventEmitter();
  
  private remainingSeconds: number;
  private timer: any;

  constructor() { }

  ngOnInit() {
   this.init();
  }

  private init() {
    this.remainingSeconds = this.goalSeconds;
    this.timer = setInterval(() => {
      if(this.remainingSeconds == 0) {
        this.finish();
        return;
      }
      this.removeOneSecond();
    }, 1000)
  }
  private removeOneSecond() {
    this.remainingSeconds = this.remainingSeconds - 1;
  }

  public finish() {
    clearInterval(this.timer);
    this.finished.next();
  }

  public reset() {
    this.init();
  }
}
