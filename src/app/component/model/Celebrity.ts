
export class Celebrity {
    id: string;
    fullName: string;
    birthDate: Date;
    deathDate: Date;
    pictureUrl: string;
    celebrityInfoUrl: string;
}