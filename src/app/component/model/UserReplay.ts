export enum Reply { 
    DEAD = "DEAD",
    ALIVE = "ALIVE",
    UNKNOWN = "UNKNOWN"
}

export class UserReply {
    celebrityId: string;
    reply: Reply;
}