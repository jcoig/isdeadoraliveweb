import { CountdownComponent } from './../countdown/countdown.component';
import { ApiService } from './../../service/api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Celebrity } from '../model/Celebrity';
import { UserReply, Reply } from '../model/UserReplay';

@Component({
  selector: 'app-in-game',
  templateUrl: './in-game.component.html',
  styleUrls: ['./in-game.component.scss']
})
export class InGameComponent implements OnInit {
  
  public Reply = Reply; // Para usar el enumerado Reply en el template..... 

  public secondsToGoal: number = 10000;
  public unknownOportunities: number = 3;
  
  public actualCelebrity: Celebrity;
  private celebritiesSucceses: UserReply [];

  @ViewChild(CountdownComponent) countdownComponent: CountdownComponent;

  constructor(private apiService: ApiService) {}

  public ngOnInit(): void {
    this.initializationPhase();
    this.resetCelebritiesSucceses();
  }
    
  public reply(reply: Reply) {
    if( reply === Reply.UNKNOWN) return this.unknown();
    this.playPhase(reply);
  }
  
  public countdownFinish() {
    alert('SE ACABO EL TIME');
    this.obtainRandomCelebrity();
    this.countdownComponent.reset();
  }

  private async initializationPhase(): Promise<any> {
    this.obtainRandomCelebrity();
    this.resetCountdown();
  }

  private async playPhase(userThinking: Reply): Promise<any> {
    const celebrityState: Reply = <Reply> await this.apiService.isDead(this.actualCelebrity.id);
    if(userThinking === celebrityState) 
      return this.userReplyIsSuccess(userThinking);
    return this.userReplyIsFail(userThinking);
  }

  private resetCelebritiesSucceses() {
    this.celebritiesSucceses = [];
  }

  private resetCountdown() {
    this.countdownComponent.reset();
  }
  
  private async obtainRandomCelebrity(): Promise<any> {
    this.actualCelebrity = <Celebrity> await this.apiService.getRandomCelebrity().toPromise();
  }

  private unknown(): void {
    this.obtainRandomCelebrity();
  }

  private userReplyIsSuccess(reply: Reply){
    const userReply: UserReply = { celebrityId: this.actualCelebrity.id, reply: reply }; 
    this.celebritiesSucceses.push(userReply);
    this.obtainRandomCelebrity();
  }

  private userReplyIsFail(reply?: Reply) {
    alert("Mal hecho");
    console.log(this.celebritiesSucceses);
  }

  
}
