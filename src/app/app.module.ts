import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartGameComponent } from './component/start-game/start-game.component';
import { InGameComponent } from './component/in-game/in-game.component';
import { HttpClientModule } from '@angular/common/http';
import { CountdownComponent } from './component/countdown/countdown.component';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    StartGameComponent,
    InGameComponent,
    CountdownComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
