import { StartGameComponent } from './component/start-game/start-game.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InGameComponent } from './component/in-game/in-game.component';

const routes: Routes = [ 
  { 
    path: '',
    redirectTo:  'start', 
    pathMatch:  'full' 
  },
  {
    path: 'start',
    component: StartGameComponent
  }, {
    path: 'game',
    component: InGameComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
